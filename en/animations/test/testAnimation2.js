
function TestAnimation2(resources)
{
	TestAnimation2.resources = resources;
}
TestAnimation2.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 325, Phaser.CANVAS, 'TestAnimation2', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this});
	},

	preload: function()
	{
		this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 325;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		
    	this.game.load.image('leftImage', TestAnimation2.resources.leftImage);
    	this.game.load.image('centerImage', TestAnimation2.resources.centerImage);
    	this.game.load.image('rightImage', TestAnimation2.resources.rightImage);
    	
    	this.game.stage.backgroundColor = '#ffffff'
    	
	},

	create: function(evt)
	{
		this.game.stage.backgroundColor = '#ffffff'
		this.parent.leftImage = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'leftImage');
		this.parent.leftImage.alpha = 1; 
		this.parent.leftImage.smoothed = false;
		this.parent.centerImage = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'centerImage');
		this.parent.centerImage.alpha = 1;
		this.parent.centerImage.smoothed = false;
		
		this.parent.rightImage = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'rightImage');
		this.parent.rightImage.alpha = 1; 
		this.parent.rightImage.smoothed = false; 
		
		var style = { font: "bold 19px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth:150, align: "center",lineSpacing: -10 };
		var style2 = { font: "bold 25px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth:150, align: "center",lineSpacing: -10 };
		this.parent.leftText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, TestAnimation2.resources.leftText, style);
		this.parent.centerText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, TestAnimation2.resources.centerText, style);
		this.parent.rightText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, TestAnimation2.resources.rightText, style);
		this.parent.buildAnimation();
		this.parent.leftText.smoothed = false;
		this.parent.centerText.smoothed = false;
		this.parent.rightText.smoothed = false;

	},

	buildAnimation: function()
	{
		
		this.rightImage.anchor.set(0.5);
		this.centerImage.anchor.set(0.5);
		this.leftImage.anchor.set(0.5);
		this.leftText.anchor.set(0.5);
		this.centerText.anchor.set(0.5);
		this.rightText.anchor.set(0.5);
		//
		
		
		this.rightText.x = this.game.world.centerX+200;
		this.rightText.y = this.game.world.centerY;
		this.rightText.originY = this.rightText.y;
		this.rightText.alpha = 0;

		this.centerText.alpha = 0;
		this.centerText.y = this.game.world.centerY;
		this.centerText.x = this.game.world.centerX;
		this.centerText.originY = this.centerText.y;
		

		this.leftText.alpha = 0;
		this.leftText.x = this.game.world.centerX-200;
		this.leftText.y = this.game.world.centerY;
		this.leftText.originY = this.leftText.y;
		////////


		this.rightImage.x = this.game.world.centerX+200;
		this.rightImage.y = this.game.world.centerY-140;
		this.rightImage.originY = this.rightImage.y;
		this.rightImage.alpha = 0;
		//this.rightImage.rotation = 5;
		
		this.centerImage.x = this.game.world.centerX;
		this.centerImage.y = this.game.world.centerY-140;
		this.centerImage.originY = this.centerImage.y;
		this.centerImage.alpha = 0;

		this.leftImage.x = this.game.world.centerX-200;
		this.leftImage.y = this.game.world.centerY-140;
		this.leftImage.originY = this.leftImage.y;
		this.leftImage.alpha = 0;

		

		
		this.leftImage.y-=150;
		this.leftImageAn = this.game.add.tween(this.leftImage).to( { alpha:1,y: this.leftImage.originY +90},800,Phaser.Easing.Quadratic.Out);

		this.leftText.y+=150;
		this.leftTextAn = this.game.add.tween(this.leftText).to( { alpha:1,y:this.leftText.originY +90},800,Phaser.Easing.Quadratic.Out);

		this.centerImage.y-=150;
		this.centerImageAn = this.game.add.tween(this.centerImage).to( { alpha:1,y:this.centerImage.originY+90},800,Phaser.Easing.Quadratic.Out);

		this.centerText.y+=150;
		this.centerTextAn = this.game.add.tween(this.centerText).to( { alpha:1,y:this.centerText.originY +90},800,Phaser.Easing.Quadratic.Out);
		
		this.rightImage.y-=150
		this.rightImageAn = this.game.add.tween(this.rightImage).to( { alpha:1,y: this.rightImage.originY +90},800,Phaser.Easing.Quadratic.Out);

		this.rightText.y+=150;
		this.rightTextAn = this.game.add.tween(this.rightText).to( {alpha:1,y:this.rightText.originY +90},800,Phaser.Easing.Quadratic.Out);
		

		this.leftImageAn.chain (this.leftTextAn,this.centerImageAn,this.centerTextAn,this.rightImageAn,this.rightTextAn);
		this.leftImageAn.start();
		
		
	},

	update: function()
	{

	},

	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}


